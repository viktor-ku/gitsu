const chalk = require('chalk')
const { spawnSync } = require('child_process')

function makeLogger({ modName }) {
  return {
    log({ message, color }) {
      console.log(`${color(modName)}: ${message}`)
    },
    succeed(message) {
      this.log({
        color: chalk.green,
        message,
      })
    },
    error(message) {
      this.log({
        color: chalk.red,
        message,
      })
    },
    info(message) {
      this.log({
        color: chalk.cyan,
        message,
      })
    }
  }
}

exports.run = function run() {
  const { modName, modPath } = process.env
  const log = makeLogger({ modName })

  const status = spawnSync('git status -s', {
    cwd: modPath,
    shell: true
  })

  if (status.stdout.length) {
    log.error('Stash or commit your local changes first')
    process.exit(1)
  }

  const checkout = spawnSync('git checkout master', {
    cwd: modPath,
    shell: true,
  })

  log.info(checkout.stderr.toString().trim())

  const fetch = spawnSync('git fetch -p', {
    cwd: modPath,
    shell: true
  })

  if (fetch.stdout.toString().trim()) {
    log.info(fetch.stdout.toString().trim())
  }

  const rebase = spawnSync('git rebase origin/master', {
    cwd: modPath,
    shell: true
  })

  log.info(rebase.stdout.toString().trim())

  process.exit(0)
}
