const fs = require('fs')
const path = require('path')
const cluster = require('cluster')

const {DOT_GIT_MODULES} = require('./lib/const')
const {die} = require('./lib/die')
const {Track} = require('./lib/Track')

const cwd = process.cwd()

function runWorker() {
  require('./worker').run()
}

function runMaster() {
  console.time('Done')
  const pathToGitModulesFile = path.join(cwd, DOT_GIT_MODULES)

  if (!fs.existsSync(pathToGitModulesFile)) {
    die({
      message: `
        File ${DOT_GIT_MODULES} not found at ${cwd}.

        Make sure to cd into the root directory that holds all git submodules
        If you think it was a mistake, open a new issue here https://gitlab.com/viktor-ku/gitsu/issues
      `
    })
  }

  const modules = fs
    .readFileSync(pathToGitModulesFile, 'utf-8')
    .split('\n')
    .filter((row) => row.trim().startsWith('path'))
    .map((row) => {
      const modName = row.split('=')[1].trim()
      const modPath = path.join(cwd, modName)

      return {
        modName,
        modPath,
      }
    })

  const modulesExited = new Track(modules.length, () => {
    console.timeEnd('Done')
  })

  modules
    .map((mod) => {
      const worker = cluster.fork(mod)

      worker.on('disconnect', () => {
        modulesExited.add()
      })

      return worker
    })
}

exports.main = function main() {
  if (cluster.isMaster) {
    return runMaster()
  } else {
    return runWorker()
  }
}
