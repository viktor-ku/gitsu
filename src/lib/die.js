exports.die = function die({ app, message }) {
  if (app) {
    app.outputHelp()
  } else {
    console.error(message)
  }

  process.exit(1)
}
