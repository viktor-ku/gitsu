exports.Track = class Track {
  constructor(goal, fn) {
    this.goal = goal
    this.cur = 0
    this.fn = fn
  }

  add(val = 1) {
    this.cur += val

    if (this.cur >= this.goal) {
      this.fn()
    }
  }
}
