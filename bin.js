#!/usr/bin/env node

const {Command} = require('commander')

const {die} = require('./src/lib/die')
const {main} = require('./src/main')
const pkg = require('./package.json')

const app = new Command()
const argv = process.argv

app
  .version(pkg.version)
  .description(pkg.description)

app
  .command('update')
  .description('update submodules')
  .action((options) => {
    main(options)
  })

app.on('command:*', () => {
  die({ app })
})

if (!argv.slice(2).length) {
  die({ app })
}

app.parse(argv)
