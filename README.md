Usage:  [options] [command]

Asynchronously update git submodules independent from the parent repository

Options:
  -V, --version  output the version number
  -h, --help     output usage information

Commands:
  update         update submodules
